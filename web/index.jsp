<%-- 
    Document   : index
    Created on : 29.5.2014, 13:18:19
    Author     : hariprasad
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%-- Set session-scoped variable to track the view user is coming from.
     This is used by the language mechanism in the Controller so that
     users view the same page when switching between English and Czech. --%>
<c:set var='view' value='/index' scope='session' />

<!DOCTYPE html>
<html>
  <head>
    <fmt:setLocale value="cs_CZ"/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="stylesheet" type="text/css" href="css/styles.css">
  <title>Go Pay Gateway</title>
  </head>
  <body>
  <%-- HTML markup starts below --%>

  <div id="singleColumn">

    <h2><fmt:message key="checkoutOrder"/></h2>

    <p><fmt:message key="checkoutTextOrder"/></p>

    <c:if test="${!empty orderFailureFlag}">
        <p class="error"><fmt:message key="orderFailureError"/></p>
    </c:if>
        
    <!--div id="header">
      Header
    </div-->
  </div>

    <div id="indexLeftColumn">
      <form id="checkoutForm" action="<c:url value='gopay'/>" method="post">
        <table id="paymentTable">
            <tr>
                <td><label for="name"><fmt:message key="name"/>:</label></td>
                <td class="inputField">
                    <input type="text"
                           size="21"
                           maxlength="45"
                           id="name"
                           name="name"
                           value="${param.name}">
                </td>
            </tr>
            <tr>
                <td><label for="surname"><fmt:message key="surname"/>:</label></td>
                <td class="inputField">
                    <input type="text"
                           size="21"
                           maxlength="45"
                           id="surname"
                           name="surname"
                           value="${param.surname}">
                </td>
            </tr>
            <tr>
                <td><label for="email"><fmt:message key="email"/>:</label></td>
                <td class="inputField">
                    <input type="text"
                           size="21"
                           maxlength="45"
                           id="email"
                           name="email"
                           value="${param.email}">
                </td>
            </tr>
            <tr>
                <td><label for="telephone"><fmt:message key="telephone"/>:</label></td>
                <td class="inputField">
                    <input type="text"
                           size="21"
                           maxlength="15"
                           id="phone"
                           name="phone"
                           value="${param.phone}">
                </td>
            </tr>
            <tr>
                <td><label for="address"><fmt:message key="address"/>:</label></td>
                <td class="inputField">
                    <input type="text"
                           size="21"
                           maxlength="45"
                           id="address"
                           name="address"
                           value="${param.address}">
                </td>
            </tr>
            <tr>
                <td><label for="city"><fmt:message key="city"/>:</label></td>
                <td class="inputField">
                    <input type="text"
                           size="21"
                           maxlength="45"
                           id="city"
                           name="city"
                           value="${param.city}">
                </td>
            </tr>
            <tr>
                <td><label for="zip"><fmt:message key="zip"/>:</label></td>
                <td class="inputField">
                    <input type="text"
                           size="8"
                           maxlength="8"
                           id="zip"
                           name="zip"
                           value="${param.zip}">
                </td>
            </tr>
            <tr>
                <td><label for="product"><fmt:message key="product"/>:</label></td>
                <td class="inputField">
                    <input type="text"
                           size="21"
                           maxlength="45"
                           id="product"
                           name="product"
                           value="${param.product}">
                </td>
            </tr>
            <tr>
                <td><label for="price"><fmt:message key="price"/>:</label></td>
                <td class="inputField">
                    <input type="text"
                           size="9"
                           maxlength="9"
                           id="price"
                           name="price"
                           value="${param.price}">
                </td>
            </tr>
            <tr>
                <td><label for="surcharge"><fmt:message key="surcharge"/>:</label></td>
                <td class="inputField">
                    <input type="text"
                           size="3"
                           maxlength="3"
                           id="surcharge"
                           name="surcharge"
                           value="${param.surcharge}">
                </td>
            </tr>
            <tr>
                <td><label for="p1"><fmt:message key="p1"/>:</label></td>
                <td class="inputField">
                    <input type="text"
                           size="21"
                           maxlength="45"
                           id="p1"
                           name="p1"
                           value="${param.p1}">
                </td>
            </tr>
            <tr>
                <td><label for="p2"><fmt:message key="p2"/>:</label></td>
                <td class="inputField">
                    <input type="text"
                           size="21"
                           maxlength="45"
                           id="p2"
                           name="p2"
                           value="${param.p2}">
                </td>
            </tr>
            <tr>
                <td><label for="p3"><fmt:message key="p3"/>:</label></td>
                <td class="inputField">
                    <input type="text"
                           size="21"
                           maxlength="45"
                           id="p3"
                           name="p3"
                           value="${param.p3}">
                </td>
            </tr>
            <tr>
                <td><label for="p4"><fmt:message key="p4"/>:</label></td>
                <td class="inputField">
                    <input type="text"
                           size="21"
                           maxlength="45"
                           id="p4"
                           name="p4"
                           value="${param.p4}">
                </td>
            </tr>
            <tr>
                <td><label for="orderNumber"><fmt:message key="orderNumber"/>:</label></td>
                <td class="inputField">
                    <input type="text"
                           size="9"
                           maxlength="9"
                           id="orderNumber"
                           name="orderNumber"
                           value="${param.orderNumber}">
                </td>
            </tr>
            <tr>
                <td class="submitButton" colspan="2">
                    <input type="submit" value="<fmt:message key='submit'/>">
                </td>
            </tr>
        </table>
      </form>
    </div>
 

      <%--div id="indexRightColumn">
        <table id="priceBox">
            <tr>
                <td><fmt:message key="subtotal"/>:</td>
                <td class="checkoutPriceColumn">
                  <fmt:formatNumber type="currency" value="${product.price}"/></td>
            </tr>
            <tr>
                <td><fmt:message key="surcharge"/>:</td>
                <td class="checkoutPriceColumn">
                    <fmt:formatNumber type="percent" value="${product.surcharge}"/></td>
            </tr>
            <tr>
                <td class="total"><fmt:message key="total"/>:</td>
                <td class="total checkoutPriceColumn">
                    <fmt:formatNumber type="currency" value="${product.amount}"/></td>
            </tr>
        </table>
      </div--%>
  </body>
</html>
