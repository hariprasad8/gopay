<%-- 
    Document   : success
    Created on : 5.6.2014, 15:20:09
    Author     : hariprasad
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%-- Set session-scoped variable to track the view user is coming from.
     This is used by the language mechanism in the Controller so that
     users view the same page when switching between English and Czech. --%>
<c:set var='view' value='/success' scope='session' />

<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="stylesheet" type="text/css" href="css/styles.css">
    <title>Success</title>
  </head>
  <body>
    <h1>Potvrzení platby</h1>
    <div id="checkoutColumn">

        <form id="checkoutForm" action="<c:url value='confirm'/>" method="post">
            <table id="checkoutTable">
              <tr>
                <td>
                <%--h2><fmt:message key="checkout"/></h2--%>
                <label for="checkoutText"><fmt:message key="checkoutText"/> </label>
                </td>
                <td class="ouputField">
                  <b>${orderNumber}</b>
                </td>
              </tr>
                <tr>
                    <td><label for="name"><fmt:message key="name"/>:</label></td>
                    <td class="ouputField">
                        ${customer.name}&nbsp${customer.surname}
                    </td>
                </tr>
                <tr>
                    <td><label for="email"><fmt:message key="email"/>:</label></td>
                    <td class="outputField">
                        ${customer.email}
                    </td>
                </tr>
                <tr>
                    <td><label for="telephone"><fmt:message key="telephone"/>:</label></td>
                    <td class="outputField">
                       ${customer.telephone}
                    </td>
                </tr>
                <tr>
                    <td><label for="address"><fmt:message key="address"/>:</label></td>
                    <td class="outputField">
                      ${customer.address}&nbsp&nbsp${customer.city}
                    </td>
                </tr>
                <tr>
                    <td class="submitButton" colspan="2">
                        <input type="submit" value="<fmt:message key='submit'/>">
                    </td>
                </tr>
            </table>
        </form>

    </div>
  </body>
</html>
