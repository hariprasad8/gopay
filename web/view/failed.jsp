<%-- 
    Document   : failed
    Created on : 30.5.2014, 15:36:16
    Author     : hariprasad
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%-- Set session-scoped variable to track the view user is coming from.
     This is used by the language mechanism in the Controller so that
     users view the same page when switching between English and Czech. --%>
<c:set var='view' value='/failed' scope='session' />

<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="stylesheet" type="text/css" href="css/styles.css">
    <title>Application failed</title>
  </head>
  <body>
    <h1>Platba nebyla úspěšně dokončena.</h1>
    <h2>Zadejte platbu znovu. V případě přetrvávajících potíží kontaktujte administrátora.</h2>
    <div id="failedColumn">

        <form id="failedForm" action="<c:url value='confirmFailed'/>" method="post">
            <table id="failedTable">
                <tr>
                    <td class="submitButton" colspan="2">
                        <input type="submit" value="<fmt:message key='submit'/>">
                    </td>
                </tr>
            </table>
        </form>

    </div>
  </body>
</html>
