/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cart;

import static java.lang.Math.round;
import java.math.BigDecimal;
import java.math.MathContext;


/** Class for storing product's attributes
 *
 * @author hariprasad
 */
public class Product {
  BigDecimal  price,
              surcharge; // increase in percents
  String  productName,
          orderNumber;

/** constructor
 *
 * @param id
 * @param price
 */
public Product(BigDecimal price, BigDecimal surcharge, String productName, String  orderNumber) {
  this.price = price;
  this.surcharge = surcharge;
  this.productName = productName;
  this.orderNumber = orderNumber;
}

  /**
   *
   * @return
   */
  public BigDecimal getPrice() {
  return price;
}

  /**
   *
   * @param price
   */
  public void setPrice(BigDecimal price) {
  this.price = price;
}
  
  /**
   *
   * @return
   */
  public BigDecimal getSurcharge() {
  return surcharge;
}

  /**
   *
   * @param surcharge
   */
  public void setSurcharge(BigDecimal surcharge) {
  this.surcharge = surcharge;
}
  
  /**
   *
   * @return
   */
  public String getProductName() {
  return productName;
}

  /**
   *
   * @param productName
   */
  public void setProductName(String productName) {
  this.productName = productName;
}
  
  /**
   *
   * @return
   */
  public String getOrderNumber() {
  return orderNumber;
}

  /**
   *
   * @param orderNumber
   */
  public void setOrderNumber(String orderNumber) {
  this.orderNumber = orderNumber;
}
  
  /** Returns amount in cents in Long
   *
   * @return
   */
  public Long getAmountInCents() {
    
    BigDecimal bd = price.multiply(surcharge.add(new BigDecimal("100")));
    bd = bd.setScale(0, BigDecimal.ROUND_HALF_UP);

    Long result = new Long(bd.longValueExact());
    //Long result = new Long(Math.round(price * (100 + surcharge)));
    return result;
  }
  
}
