/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

/** Class for setting flags for tuning and switching between testing and production URL
 *
 * @author hariprasad
 */
public class SwitchFlag {
  
// *************** for local testing purposes is true, for remote is false ***********
public static boolean testlocal = true;

// *************** for testing purposes is true, for production is false ***********
public static boolean testtest = true;

// *************** for local testing purposes to display .jsp page is true, for calling servlet directly is false ***********
//public static boolean displaypage = true;


/* possible combinations ad meaning
testlocal testtest displaypage
false     false    false        remote used production version (not recomended)
true      true     true/false   testing locally
false     true     true/false   remote testing
true      false    true/false   locally used production version (recomended)
*/


  /** general switching between "testing locally" and "production version"
   *
   * @param setglobal
   *
  */
  public static void switchFlag(boolean setglobal) {
  testlocal = setglobal;
  testtest = setglobal;
  //displaypage = setglobal;
}

  public static void setTestLocal(boolean settestlocal) {
    testlocal = settestlocal;
  }
  
  public static boolean getTestLocal() {
    return testlocal;
  }

  public static void setTestTest(boolean settesttest) {
    testtest = settesttest;
  }
  
  public static boolean getTestTest() {
    return testtest;
  }

  /*public static void setDisplayPage(boolean setdisplaypage) {
    displaypage = setdisplaypage;
  }
  
  public static boolean getDisplayPage() {
    return displaypage;
  }*/
}
