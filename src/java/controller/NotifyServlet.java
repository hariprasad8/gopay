/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import javax.ejb.EJB;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import session.CustomerOrderManager;

/**
 *
 * @author hariprasad
 */
@WebServlet(name = "NotifyServlet",
            urlPatterns = {"/gopay/notify"})

public class NotifyServlet extends HttpServlet {
  
  @EJB
  private CustomerOrderManager customerManager;
  
  public static final String PASSED = "PASSED", FAILED = "FAILED", WRONGORDER = "WRONGORDER";
 
  final int SUCESS = 200,
            INTERNAL_ERROR = 500;
  
  String checkPaymentSessionId;
  String checkEncryptedSignature;
  String checkOrderNumber;
  String checkTargetGoId;
  
  /** init method, it is called in establising of class Controlerservlet and
   *  in time after the servlet is loaded and before it begins servicing requests
   * @param servletConfig
   * @throws ServletException
   */
  @Override
  public void init(ServletConfig servletConfig) throws ServletException {
    super.init(servletConfig);
  }

  /**
   * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
   * methods.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  /*protected void processRequest(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    response.setContentType("text/html;charset=UTF-8");
    try (PrintWriter out = response.getWriter()) {
      / TODO output your page here. You may use following sample code. /
      out.println("<!DOCTYPE html>");
      out.println("<html>");
      out.println("<head>");
      out.println("<title>Servlet NotifyServlet</title>");      
      out.println("</head>");
      out.println("<body>");
      out.println("<h1>Servlet NotifyServlet at " + request.getContextPath() + "</h1>");
      out.println("</body>");
      out.println("</html>");
    }
  }*/

  // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
  /**
   * Handles the HTTP <code>GET</code> method.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    //processRequest(request, response);
        checkPaymentSessionId = request.getParameter("paymentSessionId");
        checkTargetGoId = request.getParameter("targetGoId");
        checkOrderNumber = request.getParameter("orderNumber");
        checkEncryptedSignature = request.getParameter("encryptedSignature");
        
      System.out.println("\nBEFORE CHECK [checkPaymentSessionId=" + checkPaymentSessionId + "]");
      System.out.println("\nBEFORE CHECK [checkTargetGoId=" + checkTargetGoId + "]");
      System.out.println("\nBEFORE CHECK [checkOrderNumber=" + checkOrderNumber + "]");
      System.out.println("\nBEFORE CHECK [checkEncryptedSignature=" + checkEncryptedSignature + "]");

      HashMap<String, String> result = customerManager.notifyPayment(checkPaymentSessionId,
                                                     checkOrderNumber,
                                                     checkTargetGoId,
                                                     checkEncryptedSignature);
      
      if (result.get("notificationReturn").equals(PASSED))
        response.setStatus(SUCESS);
      else 
        response.setStatus(INTERNAL_ERROR);
  }

  /**
   * Handles the HTTP <code>POST</code> method.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    response.setStatus(SUCESS);
    //processRequest(request, response);
  }

  /**
   * Returns a short description of the servlet.
   *
   * @return a String containing servlet description
   */
  @Override
  public String getServletInfo() {
    return "Servlet dedicated for backward notification.";
  }

}
