/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import cart.Product;
import cz.gopay.api.v2.EPaymentIdentity;
import cz.gopay.api.v2.EPaymentStatus;
import cz.gopay.api.v2.helper.GopayException;
import entity.ConfirmOrder;
import entity.Customer;
import entity.Gopayconfig;
import gopay.CreatePayment;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.HttpConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.rpc.ServiceException;
import session.CustomerOrderManager;

/** Main servlet controller - ControllerServlet for GoPay gateway version 1.0
 *
 * @author hariprasad
 */
@WebServlet(name = "ControllerServlet",
            loadOnStartup = 1,
            urlPatterns = {/*"/",*/
                           "/failed",
                           "/success",
                           "/gopay",
                           "/confirm",
                           "/confirmFailed"})
//@ServletSecurity( @HttpConstraint(rolesAllowed = {"GoPayYoga"}) )
public class ControllerServlet extends HttpServlet {
  final String VERSION = "1.0",
               FAILED = "/failed",
               SUCCESS = "/success",
               CONFIRM = "/confirm",
               CONFIRMFAILED = "/confirmFailed",
               ROOT = "/" /* not allowed, it breaks reading of page */,
               failedJSP = "/view/failed",
               successJSP = "/view/success",
               indexURL = "/index",
               GOPAY = "/gopay",
               postJSP = ".jsp";
          
  public static final String 
               externRootURL = "http://172.213.broadband10.iol.cz:8080/",
               internRootURL =  "http://localhost:8080/",
               APP_URL = "gopayyoga/";
  
  // *************** for local testing purposes is true, for remote false ***********
  private boolean testlocal = SwitchFlag.testlocal;
  
  Product product;
  CreatePayment payment;
  EPaymentIdentity successElement;
  Customer customer;
  ConfirmOrder confirmOrder;
  Gopayconfig gopayconfig;
  String checkPaymentSessionId;
  String checkEncryptedSignature;
  String checkOrderNumber;
  
  @EJB
  private CustomerOrderManager customerManager;


  /** init method, it is called in establising of class Controlerservlet and
   *  in time after the servlet is loaded and before it begins servicing requests
   * @param servletConfig
   * @throws ServletException
   */
  @Override
  public void init(ServletConfig servletConfig) throws ServletException {
    
    super.init(servletConfig);
    
    /*minic order number
    Long orderNumberL = System.nanoTime();
    String orderNumber = orderNumberL.toString().substring(0, 6);*/

    // obtain config parameteres
    gopayconfig = customerManager.selectConfig();
    
    // servletConfig.getServletContext().setAttribute("product", product);
  }
  
  private boolean checkSuccessElement(HttpServletRequest request) throws GopayException, ServiceException {
    
    boolean result;
        
        //after doGet
        checkPaymentSessionId = request.getParameter("paymentSessionId");
        checkEncryptedSignature = request.getParameter("encryptedSignature");
        checkOrderNumber = request.getParameter("orderNumber");
        
        System.out.println("\nBEFORE PREPARE NOTIFY [checkPaymentSessionId=" + checkPaymentSessionId + "]");
        System.out.println("\nBEFORE PREPARE NOTIFY [checkEncryptedSignature=" + checkEncryptedSignature + "]");
        System.out.println("\nBEFORE PREPARE NOTIFY [checkOrderNumber=" + checkOrderNumber + "]");
        //System.out.println("\n*** BEFORE PREPARE NOTIFY [identity.getEncryptedSignature=" + payment.getEPaymentIdentity().getEncryptedSignature() + "]");
        
        if ((successElement != null)
          &&(successElement.getPaymentSessionId().equals(new Long(checkPaymentSessionId)))
          && (successElement.getEncryptedSignature().equals(checkEncryptedSignature))
          && (successElement.getOrderNumber().equals(checkOrderNumber))) {
        result = true;
        
        System.out.println("\nBEFORE PREPARE NOTIFY, TEST SUCCESS!");

        //save to DB and finish the last operation before return to pages
        payment.prepareNotify(/*payment.getEPaymentIdentity(),*/ checkEncryptedSignature, customerManager/*, customer*/);
        // test notify for web service
        /*String result = customerManager.notifyPayment("3004352299", "", "212769", "8442323890",
          "", "", "", "", "59f27b9b4eabaa4c60ecfe292f1c1f614dd6e4f3eee0a87124cb5a0767ca1a609078109fa503d1c8b751298fe8c42681");*/
        }
        else
          result = false;
  
        return result;    
  }
  
  /**
   * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
   * methods.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  protected void processRequest(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException, GopayException, ServiceException {
    
    response.setContentType("text/html;charset=UTF-8");
    String userPath = request.getServletPath(), newPath = "";
    HttpSession session = request.getSession();
    boolean redirectGoPay = false, redirectBack = false;
    String url = "";
    boolean check;

    
    switch (userPath) {
      case GOPAY:
        newPath = indexURL; // temporarilly stay on the same page
        
          String name, surname, email, phone, address,
                 city, zip, price, productName, surcharge, p1, p2, p3, p4, orderNumber;

          name = request.getParameter("name");
          surname = request.getParameter("surname");
          email = request.getParameter("email");
          phone = request.getParameter("phone");
          address = request.getParameter("address");
          city = request.getParameter("city");
          zip = request.getParameter("zip");
          price = request.getParameter("price");
          productName = request.getParameter("product");
          surcharge = request.getParameter("surcharge");
          p1 = request.getParameter("p1");
          p2 = request.getParameter("p2");
          p3 = request.getParameter("p3");
          p4 = request.getParameter("p4");
          orderNumber = request.getParameter("orderNumber");
          
          System.out.println("GETTING PARAMETERS name, surname, email, phone, address," +
                             " city, zip, price, productName, surcharge, orderNumber: " +
                             name + ", " + surname + ", " + email + ", " + phone + ", " + address +
                             ", " + city + ", " + zip + ", " + price + ", " + productName +
                             ", " + surcharge + ", " + orderNumber);

        // save Customer data to the database
        customer = customerManager.addCustomer(
                            name, surname,
                            email, phone,
                            address, city, zip);
        
        // product initialization
        product = new Product(new BigDecimal(price), new BigDecimal(surcharge), productName, orderNumber);
        
        session.setAttribute("customer", customer);
        session.setAttribute("orderNumber", product.getOrderNumber());

        // prepare and call GoPay
        payment = new CreatePayment(gopayconfig);
                
        System.out.println("A new ORDER NUMBER: " + product.getOrderNumber() + "***");

        /* valid redirect for success and failed redirect back to my page */
        EPaymentStatus status = payment.createPayment(/*newPath + postJSP"http://zive.cz",
                              /*failedJSP + postJSP"http://lidovky.cz",*/
                              product.getProductName(),
                              product.getOrderNumber(),
                              product.getAmountInCents(),
                              name, surname, email, phone, address, city, zip,
                              p1, p2, p3, p4);
        
			  System.out.println("\nCREATE [paymentSessionId=" + status.getPaymentSessionId() + "]");
			  System.out.println("\nCREATE [encryptedSignature=" + status.getEncryptedSignature() + "]");
        
        /* Save Customer Order to DB before calling redirect to protect notification fail, which is asynchronous */
        /*status.getEncryptedSignature() encrypted Signature in status is different than ncrypted Signature in prepare notify*/
        confirmOrder = customerManager.addConfirmOrder(
                                        status,
                                        customer);

			
			  //assembly redirect to payment gateway GoPay
			  String redirect = payment.redirectToGW(status.getPaymentSessionId());
        redirectGoPay = true;
        newPath = redirect /*"http://www.seznam.cz"*/;
        
        
        //redirect to GoPay
        response.sendRedirect(newPath);
        System.out.println("\nAFTER REDIRECT STATUS AND PATH [status=" + response.getStatus() + " " + newPath+ "]");
        
        //after redirect assembly patch to success, failed URL back redirect
        successElement = payment.successRedirect(status);
        System.out.println("\nSUCCESS REDIRECT [paymentSessionId=" + successElement.getPaymentSessionId() + "]");
      break;
      case FAILED:
        check = checkSuccessElement(request);
        
        // this is not state from GoPay (FAILED_URL_STATE)
        String failedUrlStr = payment.failedUrl(successElement);
        System.out.println("\nFAILEDURL in ControllerServlet SUCCESS  [successUrlStr=\"" + failedUrlStr + "\"]");
        customerManager.updateConfirmOrderStatus(confirmOrder, null, failedUrlStr);
        newPath = failedJSP;
      break;
      case SUCCESS:
        check = checkSuccessElement(request);
        
        String successUrlStr = payment.successUrl(successElement);
        System.out.println("\nSUCCESS URL in ControllerServlet SUCCESS  [successUrlStr=\"" + successUrlStr + "\"]");
        customerManager.updateConfirmOrderStatus(confirmOrder, null, successUrlStr);

        // testing before using success.jsp/failed.jsp
        /*Customer cu = (Customer) session.getAttribute("customer");
        System.out.println("\n*****ControllerServlet SUCCESS  [Customer name=\"" + cu.getName() + "\"]");
        String ord = (String) session.getAttribute("orderNumber");
        System.out.println("\n*****ControllerServlet SUCCESS  [orderNumber=\"" + ord + "\"]");*/

        if (check)
          newPath = successJSP;
        else
          newPath = failedJSP;
      break;
      // redirect back to main page index.jsp
      case CONFIRM:
        newPath = indexURL;        
        redirectBack = true;
      break;
      case CONFIRMFAILED:
        newPath = indexURL;
        redirectBack = true;
      break;
      case ROOT:
      break;
    }
    
    /*try (PrintWriter out = response.getWriter()) {
      * TODO output your page here. You may use following sample code. *
      out.println("<!DOCTYPE html>");
      out.println("<html>");
      out.println("<head>");
      out.println("<title>Servlet ControllerServlet</title>");      
      out.println("</head>");
      out.println("<body>");
      out.println("<h1>Servlet ControllerServlet at " + request.getContextPath() + "</h1>");
      out.println("</body>");
      out.println("</html>");
    }*/
    
    // use RequestDispatcher to forward request internally
    if (redirectGoPay == true)
      ; // do nothing
    else if (redirectBack == true) {
      // redirect bact to first page
      String redirectURL = null;
      if (testlocal)
        redirectURL = internRootURL + APP_URL;
      else
        redirectURL = externRootURL + APP_URL;
      //session.invalidate();
      response.sendRedirect(redirectURL);
    }
    else {
      url = null;
      if (userPath.equals(indexURL) || userPath.equals(ROOT))
        url = indexURL + postJSP;
      else
        url = newPath + postJSP;

      try {
        request.getRequestDispatcher(url).forward(request, response);
      } catch (Exception ex) {
          ex.printStackTrace();
      }
    }
  }

  // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
  /**
   * Handles the HTTP <code>GET</code> method.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    try {
      processRequest(request, response);
    } catch (GopayException ex) {
      Logger.getLogger(ControllerServlet.class.getName()).log(Level.SEVERE, null, ex);
    } catch (ServiceException ex) {
      Logger.getLogger(ControllerServlet.class.getName()).log(Level.SEVERE, null, ex);
    }
  }

  /**
   * Handles the HTTP <code>POST</code> method.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    try {
      processRequest(request, response);
    } catch (GopayException ex) {
      Logger.getLogger(ControllerServlet.class.getName()).log(Level.SEVERE, null, ex);
    } catch (ServiceException ex) {
      Logger.getLogger(ControllerServlet.class.getName()).log(Level.SEVERE, null, ex);
    }
  }

  /**
   * Returns a short description of the servlet.
   *
   * @return a String containing servlet description
   */
  @Override
  public String getServletInfo() {
    return "Servlet for creating payment.";
  }

}
