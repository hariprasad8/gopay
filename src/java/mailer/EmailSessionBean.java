/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mailer;

import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

/**
 *
 * @author hariprasad, accoring tutorial
 */
@Stateless
public class EmailSessionBean {
    
   private int port = 465;
   private String host = "smtp.gmail.com";
   private String from = "petr.hariprasad@gmail.com";
   private boolean auth = true;
   private String username = "petr.hariprasad@gmail.com";
   private String password = "Shankar108";
   private Protocol protocol = Protocol.SMTPS;
   private boolean debug = false;
   private String charset = "utf-8";
   private static final String HTML_CONTENT_TYPE = "text/html; charset=UTF-8";
   private static final String TEXT_CONTENT_TYPE = "text/plain; charset=UTF-8";

     /**
   * @return String - HTML doc
   */
  protected String buildHtmlMsg(String to, String subject, String body, String orderNumber) {
    
    String htmlContent;
    htmlContent = "<!DOCTYPE HTML PUBLIC><br>";
    htmlContent += "<html><br>";
    htmlContent += "<head><br>";
    //htmlContent +=  "<META HTTP-EQUIV=\"CONTENT-TYPE\" CONTENT=\"text/html; charset=utf-8\"><br>";
    htmlContent += "<meta http-equiv=\"content-type\" content=\"text/html; charset=UTF-8\">";
    htmlContent += "<title>Potvrzení platby</title><br>";            
    htmlContent += "</head><br>";
    htmlContent += "<body><br>";
    htmlContent += "<h1>Platba číslo objednávky " + orderNumber + "</h1>";
    String outtext = body + /*"Charset: " + charset +*/""; 
    htmlContent += "<p>"
                      + outtext + "</p><br>";
    htmlContent += "</body><br>";
    htmlContent += "</html><br>";
    
    /*"<head>\n" + *"<meta charset=" + charset + ">\n" +* "</head>" + "<html><h1>" + subject + "</h1><p>" + body +
    "</p></html>";*/
    //htmlContent = "<html><h1>Hi</h1><p>Nice to meet you!</p></html>";
    return htmlContent;
  }
  
  public void sendEmail(String to, String subject, String body, String orderNumber){
        
    Properties props = new Properties();
    props.put("mail.smtp.host", host);
    props.put("mail.smtp.port", port);
    
    switch (protocol) {
      case SMTPS:
        props.put("mail.smtp.ssl.enable", true);
        break;
      case TLS:
        props.put("mail.smtp.starttls.enable", true);
        break;  
    }
      
    Authenticator authenticator = null;
    if (auth) {
      props.put("mail.smtp.auth", true);
      authenticator = new Authenticator() {
        private PasswordAuthentication pa = new PasswordAuthentication(username, password);
        @Override
        public PasswordAuthentication getPasswordAuthentication() {
            return pa;
        }
      };
    }
      
    Session session = Session.getInstance(props, authenticator);
    session.setDebug(debug);
      
    MimeMessage message = new MimeMessage(session);
      try {
        //String encodingOptions = "text/html; charset=UTF-8";
        //message.setHeader("Content-Type", encodingOptions);
        message.setFrom(new InternetAddress(from));
        InternetAddress[] address = {new InternetAddress(to)};
        message.setRecipients(Message.RecipientType.TO, address);
        System.out.println(subject);
        //HandleUTF8Charset hch = new HandleUTF8Charset();
        //byte[] sbj = subject.getBytes(charset);
        //byte[] sbj = hch.encodeUTF8(subject);
        message.setSubject(subject, charset);
        message.setSentDate(new Date());
        //message.setText(body);
        Multipart multipart = new MimeMultipart("alternative");

        MimeBodyPart textPart = new MimeBodyPart();
        String textContent = body; /*"Hi, Nice to meet you!"*/
        //textPart.setText(to, charset);
        textPart.setContent(textContent, TEXT_CONTENT_TYPE);

        MimeBodyPart htmlPart = new MimeBodyPart();
        String htmlContent = buildHtmlMsg(to, subject, body, orderNumber);
        htmlPart.setContent(htmlContent, HTML_CONTENT_TYPE);
          
          multipart.addBodyPart(textPart);
          multipart.addBodyPart(htmlPart);
          message.setContent(multipart);

          Transport.send(message);
        }
        catch (MessagingException ex) {
          ex.printStackTrace();
        }
        /*} catch (UnsupportedEncodingException ex) {
           Logger.getLogger(EmailSessionBean.class.getName()).log(Level.SEVERE, null, ex);
       }*/
    }
}
