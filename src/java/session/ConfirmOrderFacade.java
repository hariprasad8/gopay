/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package session;

import entity.ConfirmOrder;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

/**
 *
 * @author root
 */
@Stateless
public class ConfirmOrderFacade extends AbstractFacade<ConfirmOrder> {
  @PersistenceContext(unitName = "GoPayYogaPU")
  private EntityManager em;

  @Override
  protected EntityManager getEntityManager() {
    return em;
  }

  public ConfirmOrderFacade() {
    super(ConfirmOrder.class);
  }
  
  // manually created
  // in this implementation, there is only one order per customer
  // the data model however allows for multiple orders per customer
  public ConfirmOrder findByOrderNumber(String orderNumber) {
    ConfirmOrder confirmOrder;
    try {
      confirmOrder = (ConfirmOrder) em.createNamedQuery("ConfirmOrder.findByOrderNumber").setParameter("orderNumber", orderNumber).getSingleResult();
    }
    catch (NoResultException nre) {
      System.out.println("For order number: " + orderNumber + " is no  key in CustOrder table. It can be caused by asynchronous calling of notification. Wait for next attempt and check the result in table.");
      confirmOrder = null;
    }
    catch (Exception ex) {
      ex.printStackTrace();
      confirmOrder = null;
    }

    return confirmOrder;
  }
  
}
