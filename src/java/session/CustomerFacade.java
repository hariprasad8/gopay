/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package session;

import entity.Customer;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

/**
 *
 * @author hariprasad
 */
@Stateless
public class CustomerFacade extends AbstractFacade<Customer> {
  @PersistenceContext(unitName = "GoPayYogaPU")
  private EntityManager em;

  @Override
  protected EntityManager getEntityManager() {
    return em;
  }

  public CustomerFacade() {
    super(Customer.class);
  }
  
  // manually created
  // find customer by id
  public Customer findById(Integer Id) {
    Customer customer;
    try {
      customer = (Customer) em.createNamedQuery("Customer.findById").setParameter("id", Id).getSingleResult();
    }
    catch (NoResultException nre) {
      System.out.println("For order number: " + Id + " is no  key in Customer table.");
      customer = null;
    }
    catch (Exception ex) {
      ex.printStackTrace();
      customer = null;
    }
    return customer;
  }
  
}
