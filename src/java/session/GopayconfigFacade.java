/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package session;

import entity.Gopayconfig;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author hariprasad
 */
@Stateless
public class GopayconfigFacade extends AbstractFacade<Gopayconfig> {
  @PersistenceContext(unitName = "GoPayYogaPU")
  private EntityManager em;

  @Override
  protected EntityManager getEntityManager() {
    return em;
  }

  public GopayconfigFacade() {
    super(Gopayconfig.class);
  }
  
}
