/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package session;

import controller.NotifyServlet;
import cz.gopay.api.v2.EPaymentIdentity;
import cz.gopay.api.v2.EPaymentSessionInfo;
import cz.gopay.api.v2.EPaymentStatus;
import cz.gopay.api.v2.axis.AxisEPaymentProviderV2;
import cz.gopay.api.v2.axis.AxisEPaymentProviderV2ServiceLocator;
import cz.gopay.api.v2.helper.GopayException;
import cz.gopay.api.v2.helper.GopayHelper;
import entity.ConfirmOrder;
import entity.Customer;
import entity.Gopayconfig;
import gopay.CreatePayment;
import static gopay.CreatePayment.CURRENCY;
import java.net.URL;
import java.util.Date;
import java.util.HashMap;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import mailer.EmailSessionBean;

/**
 *
 * @author root
 */
@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class CustomerOrderManager {
  //final String PASSED = "PASSED", FAILED = "FAILED";
    
  @PersistenceContext(unitName = "GoPayYogaPU")
  private EntityManager em;
  
  @Resource
  private SessionContext context;
  
  // this annotation - EJB is way how to approach to the methods of entity classes and named queries
  @EJB
  private CustomerFacade customerFacade;
  @EJB
  private ConfirmOrderFacade confirmOrderFacade;
  @EJB
  private GopayconfigFacade gopayconfigFacade;
  
  ConfirmOrder confirmOrder;

  // Add business logic below. (Right-click in editor and choose "Insert Code > Add Business Method")
  /**
   *
   * @param name
   * @param surname
   * @param email
   * @param phone
   * @param address
   * @param city
   * @return
   */
    public Customer addCustomer(String name, String surname, String email,
                               String phone, String address, String city, String zip) {
  
    Customer customer = new Customer();
    customer.setName(name);
    customer.setSurname(surname);
    customer.setEmail(email);
    customer.setTelephone(phone);
    customer.setAddress(address);
    customer.setCity(city);
    customer.setZip(zip);
    customer.setDatum(new Date());
    
    em.persist(customer);

    return customer;
  }
    
  /**
   *
   * @param productName
   * @param orderNumber
   * @param totalPrice
   * @param currency
   * @param paymentSessionId
   * @param encryptedSignature - prepared for notification
   * @param customer
   * @param p1
   * @param p2
   * @param p3
   * @param p4
   * @param notificationStatus
   * @param paymentStatus
   * @return
   */
  public ConfirmOrder addConfirmOrder(String productName, String orderNumber, Long totalPrice,
                      String currency, Long paymentSessionId, String encryptedSignature,
                      String p1, String p2, String p3, String p4, String paymentStatus,
                      Integer notificationStatus,
                      Customer customer) {
    confirmOrder = new ConfirmOrder();
    
    confirmOrder.setProductName(productName);
    confirmOrder.setOrderNumber(orderNumber);
    confirmOrder.setTotalPrice(totalPrice);
    confirmOrder.setCurrency(currency);
    confirmOrder.setPaymentSessionId(paymentSessionId);
    confirmOrder.setEncryptedSignature(encryptedSignature);
    confirmOrder.setP1(p1);
    confirmOrder.setP2(p2);
    confirmOrder.setP3(p3);
    confirmOrder.setP4(p4);
    confirmOrder.setPaymentStatus(paymentStatus);
    confirmOrder.setNotificationStatus(notificationStatus);
    confirmOrder.setDatum(new Date());
    confirmOrder.setCustomerId(customer);
            
    em.persist(confirmOrder);
  
  return confirmOrder;
  }
  
  public ConfirmOrder addConfirmOrder(EPaymentStatus status,
                                      Customer customer) {
    ConfirmOrder locConfirmOrder = addConfirmOrder(
                                   status.getProductName(),
                                   status.getOrderNumber(),
                                   status.getTotalPrice(),
                                   status.getCurrency(),
                                   status.getPaymentSessionId(),
                                   null, /*status.getEncryptedSignature() -- this is not proper signature*/
                                   status.getP1(), status.getP2(), status.getP3(), status.getP4(),
                                   null, null, customer);
  return locConfirmOrder;
  }
  
  public ConfirmOrder updateConfirmOrder(/*String paymentStatus,*/ String encryptedSignature) {
    
    try {
      if (confirmOrder == null)
          throw new Exception("*** Object ConfirmOrder does not exists ***");
      else {
        //confirmOrder.setPaymentStatus(paymentStatus);
        confirmOrder.setEncryptedSignature(encryptedSignature);
      }
    }
    catch (Exception ex) {
    }

    try {
    // update confirmorder
      if (em == null)
          throw new Exception("*** Enterprise Manager not Estblished ***");
      else if (confirmOrder != null)
        em.merge(confirmOrder);
    }
    catch (Exception ex) {
    }
  
    return confirmOrder;
  }
  
  /** Used for notifyPayment and sucessURL methods, it can be called asynchronously
   *
   * @param confirmOrder
   * @param notificationStatus
   * @param notificationText
   */
  public void updateConfirmOrderStatus(ConfirmOrder confirmOrder,
                                             Integer notificationStatus,
                                             String notificationText) {
    
    try {
      if (confirmOrder == null)
          throw new Exception("*** Object ConfirmOrder does not exists ***");
      else {
        //confirmOrder.setPaymentStatus(paymentStatus);
        confirmOrder.setNotificationStatus(notificationStatus);
        confirmOrder.setPaymentStatus(notificationText);
      }
    }
    catch (Exception ex) {
    }

    try {
    // update confirmorder
      if (em == null)
          throw new Exception("*** Enterprise Manager not Estblished ***");
      else if (confirmOrder != null)
        em.merge(confirmOrder);
    }
    catch (Exception ex) {
    }
  }
  
  public Gopayconfig selectConfig() {
    // get first element from List
    return gopayconfigFacade.findAll().get(0);
  }
  
  /** Http Notification is called asynchronously, method cannot relay on any global attributes
   * and stateful beans (only stateless)
   *
   * @param  paymentSessionId String
   * @param  parentPaymentSessionId String
   * @param  orderNumber String
   * @param  targetGoId String
   * @param  p1 String
   * @param  p2 String
   * @param  p3 String
   * @param  p4 String
   * @param encryptedSignature String
   * @return GoPayHash (HashMap inside)
   * key                        value
   * notificationResult         1 STATE_PAID (correct, passed, all right)
   *                            0 - FAILED (not known state, untreated state)
   *                            -1 STATE_CANCELED (wrong order, order canceled) 
   *                            -2 STATE_PAYMENT_METHOD_CHOSEN (payment was created, but not ended)
   *                            2 STATE_REFUNDED (this state is prepared for refund, but not implemented)
   *                            -3 STATE_TIMEOUTED (timeout)
   * notificationDescription    PASSED, FAILED, WRONGORDER - number of order is wrong
   * universalId                id of payment (order number)
   */
  public HashMap<String, String> notifyPayment(String paymentSessionId,
                                  String orderNumber,
                                  String targetGoId,
                                  String encryptedSignature) {
  Long ESHOP_GOID; //  zadejte vase testovaci EshopGoID
	String SECURE_KEY; //zadejte vas testovaci secret
      
  final String PASSED = NotifyServlet.PASSED,
               FAILED = NotifyServlet.FAILED,
               WRONGORDER = NotifyServlet.WRONGORDER;
    
    HashMap<String, String> notifyBack = new HashMap();

    Integer notificationStatus = new Integer(0);
    String notificationText = null;
    String notificationReturn = null; // PASSED, FAILED, WRONGORDER
    String notificationOrderNumber = null;
    
    // read order from table custorder
    ConfirmOrder confirmOrder =  confirmOrderFacade.findByOrderNumber(orderNumber);
    // customer from customer table
    Customer customer = confirmOrder.getCustomerId(); //customerFacade.find(confirmOrder.getCustomerId());

    //read shop GoId from table table gopayconfig
    Gopayconfig gopayconfig = gopayconfigFacade.findAll().get(0);
    
    ESHOP_GOID = gopayconfig.getGoid();
    SECURE_KEY = gopayconfig.getSecurekey();
    
    EPaymentIdentity identity = new EPaymentIdentity();
    identity.setOrderNumber(orderNumber);
    identity.setPaymentSessionId(new Long(paymentSessionId));
    identity.setTargetGoId(ESHOP_GOID);
    identity.setP1(confirmOrder.getP1());
    identity.setP2(confirmOrder.getP2());
    identity.setP3(confirmOrder.getP3());
    identity.setP4(confirmOrder.getP4());
    identity.setEncryptedSignature(encryptedSignature);

      try {

        AxisEPaymentProviderV2 provider = new AxisEPaymentProviderV2ServiceLocator().getEPaymentServiceV2(new URL(CreatePayment.actual_WS));

        /*kontrola parametru z redirectu - podpis, vazba na spravnou objednavku
          if there is no exception, everything is all right*/ 
        boolean checkPaymentIdentity = GopayHelper.checkPaymentIdentity(identity, ESHOP_GOID, orderNumber, SECURE_KEY);	    

        EPaymentSessionInfo info = new EPaymentSessionInfo();
        info.setTargetGoId(ESHOP_GOID);
        info.setPaymentSessionId(identity.getPaymentSessionId());
        GopayHelper.signEPaymentSession(info, SECURE_KEY);
        //GopayHelper.signEPaymentIdentity(identity, SECURE_KEY);

        EPaymentStatus status = provider.paymentStatus(info);
      
        if (!EPaymentStatus.RESULT_CALL_COMPLETED.equals(status.getResult())) {
          throw new GopayException(GopayException.Reason.OTHER,
              "status failed [" + status.getResultDescription() + "] ");
        }
        //kontrola parametru ve stavu platby
        boolean checkPaymentStatus = GopayHelper.checkPaymentStatus(status, 
            ESHOP_GOID, 
            confirmOrder.getOrderNumber(), 
            confirmOrder.getTotalPrice(), 
            CURRENCY, 
            confirmOrder.getProductName(), 
            SECURE_KEY);

        System.out.println("ORDER NUMBER IN notifyPayment: [" + orderNumber + "]");
        System.out.println("PAYMENT STATUS IN notifyPayment [paymentSessionId=" + status.getPaymentSessionId() + ", state="  + status.getSessionState() + "]");
        // Right signature for Notification
        System.out.println("*** ENCRYPTED SIGNATURE IDENTITY IN notifyPayment: [" + identity.getEncryptedSignature() + "]");
        System.out.println("ENCRYPTED SIGNATURE STATUS IN notifyPayment: [" + status.getEncryptedSignature() + "]");

        EmailSessionBean esb = new EmailSessionBean();

        /* save Customer Order to DB
        customerManager.addConfirmOrder(status.getProductName(), status.getOrderNumber(),
                                        status.getTotalPrice().intValue(), status.getCurrency(),
                                        status.getPaymentSessionId(), /successElement/identity.getEncryptedSignature(), //prepared Encrypted Signature for notification
                                        customerId,
                                        status.getP1(), status.getP2(), status.getP3(), status.getP4(),
                                        status.getSessionState());*/
        
        String checkStatus = status.getSessionState();
        
        notificationOrderNumber = orderNumber;
        
        if (checkPaymentIdentity && checkPaymentStatus) {
          if (EPaymentStatus.STATE_PAID.equals(checkStatus)) {
          /*esb.sendEmail(customer.getEmail(), "Potvrzení objednávky číslo " + orderNumber,
                                             "Dobrý den, <br><br>vaše platba v ceně " + payment.getTotalPrice()/100 + " Kč<br>" +""
                                             + "na jméno " + customer.getFirstName() + " " + customer.getLastName() + "<br>"
                                             + "za objednávku: " + payment.getProductName() + " byla úspěšně odeslána ke zpracování.<br>", orderNumber);*/
          //overeni stavu objednavky v ramci eshop
          //a) objednavka neni uhrazena => uhrada objednavky
          //b) objednavka je uhrazena => OK (HttpServletResponse.SC_OK = 200) - znovu jiz nehradime - notifikace je dorucovana, 
          //dokud E-shop nevrati HTTP result code 200
          //c) objednavka v jinem stavu => zalogovani situace - informovani spravce systemu
          //HTTP result code 200 - notifikaci dorucujeme, dokud neni prijata
                
          notificationStatus = 1;
          notificationText = checkStatus;
          notificationReturn = PASSED;

          System.out.println("*** Success queries ***");
          System.out.println("PaymentSessionId: " + confirmOrder.getPaymentSessionId());
          System.out.println("OrderNumber: " + confirmOrder.getOrderNumber());
          System.out.println("Goid: " + gopayconfig.getGoid());

        } else if (EPaymentStatus.STATE_PAYMENT_METHOD_CHOSEN.equals(checkStatus)) {
          esb.sendEmail(customer.getEmail(), "Platba převodem " + status.getOrderNumber(),
                                             "Dobrý den, <br><br>vaše platba v ceně " + confirmOrder.getTotalPrice()/100 + "Kč<br>" +""
                                             + "na jméno " + customer.getName() + " " + customer.getSurname() + "<br>"
                                             + "za objednávku: " + confirmOrder.getProductName() + " bude zaplacena převodem.<br>", orderNumber);
          //kontrola stavu objednavky v E-shopu
          //a) objednavku je mozne zrusit - zruseni objednavky
          //b) objednavku neni mozne zrusit - zalogovani situace - informovani spravce
          //HTTP result code 500 - chybny status, notifikaci dale nedorucujeme
          notificationStatus = -2;
          notificationText = checkStatus;
          notificationReturn = FAILED;
          
        } else if (EPaymentStatus.STATE_CANCELED.equals(status.getSessionState())) {
          esb.sendEmail(customer.getEmail(), "Zrušení objednávky číslo " + status.getOrderNumber(),
                                             "Dobrý den, <br><br>vaše platba v ceně " + confirmOrder.getTotalPrice()/100 + "Kč<br>" +""
                                             + "na jméno " + customer.getName() + " " + customer.getSurname() + "<br>"
                                             + "za objednávku: " + confirmOrder.getProductName() + " byla odmítnuta.<br>", orderNumber);
          //kontrola stavu objednavky v E-shopu
          //a) objednavku je mozne zrusit - zruseni objednavky
          //b) objednavku neni mozne zrusit - zalogovani situace - informovani spravce
          //HTTP result code 200 - notifikaci dorucujeme, dokud neni prijata
          notificationStatus = -1;
          notificationText = checkStatus;
          notificationReturn = PASSED;

        } else if (EPaymentStatus.STATE_REFUNDED.equals(status.getSessionState())) {
          esb.sendEmail(customer.getEmail(), "Vrácení peněz za objednávku číslo " + status.getOrderNumber(),
                                             "Dobrý den, <br><br>vaše platba v ceně " + confirmOrder.getTotalPrice()/100 + "Kč<br>" +""
                                             + "na jméno " + customer.getName() + " " + customer.getSurname() + "<br>"
                                             + "za objednávku: " + confirmOrder.getProductName() + " Vám byla navrácena.<br>"
                                             + "Pokud se zaúčtování výše uvedené částky Vám neobjeví do tří dnů,<br>"
                                             + "na Vašem účtu neprodleně nás kontaktujte.", orderNumber);
          //storno platby na zaklade pozadavku obchodnika, nebo zakaznika
          //HTTP result code 200 - notifikaci dorucujeme, dokud neni prijata
          notificationStatus = 2;
          notificationText = checkStatus;
          notificationReturn = PASSED;

        } else if (EPaymentStatus.STATE_TIMEOUTED.equals(status.getSessionState())) {
          esb.sendEmail(customer.getEmail(), "Vypršení lhůty " + status.getOrderNumber(),
                                             "Dobrý den, <br><br>vaše platba v ceně " + confirmOrder.getTotalPrice()/100 + "Kč<br>" +""
                                             + "na jméno " + customer.getName() + " " + customer.getSurname() + "<br>"
                                             + "za objednávku: " + confirmOrder.getProductName() + " Nebyla uskutečněna.<br>"
                                             + "Došlo k překroční časového limitu.<br>"
                                             + "Založte novou objednávku.", orderNumber);

          notificationStatus = -3;
          notificationText = checkStatus;
          notificationReturn = PASSED;
        }
        else {
          System.out.println("***** Při kontrole návratové hodnoty nastal neošetřený stav: " + status.getSessionState());
          //zalogovani situace - informovani spravce
          //HTTP result code 500 - chybny status, notifikaci dale nedorucujeme
          notificationStatus = 0;
          notificationText = checkStatus;
          notificationReturn = FAILED;
          throw new Exception();
       }
      }
     } catch (Exception e) {
        System.out.println("Exception, non known state E=[" + e + "]");
      }  
      /*if ((new Long(paymentSessionId).equals(confirmOrder.getPaymentSessionId()))
         && (encryptedSignature.equals(confirmOrder.getEncryptedSignature()))
         && (orderNumber.equals(confirmOrder.getOrderNumber()))
         && ((new Long(targetGoId)).equals(gopayconfig.getGoid()))) {

      }
      else {
        notificationStatus = new Integer(0);
        notificationText = FAILED;
        notificationOrderNumber = orderNumber;      
      }*/
      
      // if order exists do update
      updateConfirmOrderStatus(confirmOrder, notificationStatus, notificationText);
   
      notifyBack.put("notificationResult", notificationStatus.toString());
      notifyBack.put("notificationReturn", notificationReturn);
      notifyBack.put("notificationDescription", notificationText);
      notifyBack.put("universalId", notificationOrderNumber);

      /*hs.put("notificationResult", notificationStatus.toString());
      hs.put("notificationDescription", notificationText);
      hs.put("universalId", notificationOrderNumber);*/
      System.out.println("*** HTTP Notification finished: " + notificationText + " ****");
      
  return notifyBack;
  }
}
