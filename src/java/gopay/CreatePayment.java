/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gopay;

import controller.ControllerServlet;
import controller.SwitchFlag;
import cz.gopay.api.v2.ECustomerData;
import cz.gopay.api.v2.EPaymentCommand;
import cz.gopay.api.v2.EPaymentIdentity;
import cz.gopay.api.v2.EPaymentSessionInfo;
import cz.gopay.api.v2.EPaymentStatus;
import cz.gopay.api.v2.axis.AxisEPaymentProviderV2;
import cz.gopay.api.v2.axis.AxisEPaymentProviderV2ServiceLocator;
import cz.gopay.api.v2.helper.CountryCode;
import cz.gopay.api.v2.helper.EPaymentConstants;
import cz.gopay.api.v2.helper.GopayException;
import cz.gopay.api.v2.helper.GopayHelper;
import entity.Customer;
import entity.Gopayconfig;
import static java.lang.Compiler.command;
import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.RemoteException;
import javax.xml.rpc.ServiceException;
import mailer.EmailSessionBean;
import session.CustomerOrderManager;



/** Class create Payment for creating GoPay payment
 *
 * @author hariprasad
 *
 */
public class CreatePayment {
  EPaymentCommand paymentCommand = null;
  ECustomerData customerData = null;
  
  private Long ESHOP_GOID; //  zadejte vase testovaci EshopGoID
	private String SECURE_KEY; //zadejte vas testovaci secret

	public static final String CURRENCY = "CZK";
	
	public static final String REMOTE_URL = ControllerServlet.externRootURL + ControllerServlet.APP_URL;
  public static final String LOCAL_URL = ControllerServlet.internRootURL + ControllerServlet.APP_URL;
  String success_URL /*"http://172.213.broadband10.iol.cz:27930/gopayyoga/success"*/;
	String failed_URL /*"http://172.213.broadband10.iol.cz:27930/gopayyoga/failed"*/;
  public static final String SUCCESS_URL = "success"/*"http://172.213.broadband10.iol.cz:27930/gopayyoga/success"*/;
	public static final String FAILED_URL = "failed" /*"http://172.213.broadband10.iol.cz:27930/gopayyoga/failed"*/;
	public static final String SUCCESS = "successURL";
	public static final String FAILED = "failedURL";
  
// *************** for local testing purposes is true, for remote false ***********
private boolean testlocal = SwitchFlag.testlocal;

// *************** for testing purposes is true, for production is false ***********
public boolean testtest = SwitchFlag.testtest;

	
	////viz Kody platebnich metod - integracni manual;
	public static final String ALLOWED_PAYMENT_CHANNELS = GopayHelper.concatPaymentChannels(
            "eu_gp_u",
            "eu_cg",
            "cz_kb", 
            "cz_rb",
            "cz_mb",
            "cz_fb",
            "eu_bank",
            "eu_gp_w",
            "cz_mp",
            "eu_pr_sms");
  
	//viz Kody platebnich metod - integracni manual;
	public static final String DEFAULT_PAYMENT_CHANNELS = "eu_gp_u";
		
	public static final String CUST_COUNTRY = String.valueOf(CountryCode.CZE);
		
	/*public static final String P1 = String.valueOf("P1");
	public static final String P2 = String.valueOf("P2");
	public static final String P3 = String.valueOf("P3");
	public static final String P4 = String.valueOf("P4 příliš žluťoučký kůň pěl "
          + "úděsné ódy o tom, že sveřepí šakali zavile vyli na bílý měsic.");*/
	
	//viz Kody jazyka - integracni manual;
	public static final String LANG = String.valueOf("CS");
  
  String orderId; // Order identification
  Long totalPriceCents;
  String productName;
  
  EPaymentIdentity successElement;
  AxisEPaymentProviderV2 provider;
	
	public static final String WS_TEST = EPaymentConstants.GOPAY_WS_ENDPOINT_TEST; //Zadejte spravne URL
	public static final String REDIRECT_TEST = EPaymentConstants.GOPAY_FULL_TEST;  //Zadejte spravne URL

  public static final String WS_FULL = EPaymentConstants.GOPAY_WS_ENDPOINT; //Zadejte spravne URL
	public static final String REDIRECT_FULL = EPaymentConstants.GOPAY_FULL;  //Zadejte spravne URL
  
  public static String actual_WS, actual_REDIRECT;
  
  // noname block
  {
    if (testlocal) {
      success_URL = LOCAL_URL + SUCCESS_URL;
      failed_URL = LOCAL_URL + FAILED_URL;
    }
    else {
      success_URL = REMOTE_URL + SUCCESS_URL;
      failed_URL = REMOTE_URL + FAILED_URL;
    }
    if (testtest) {
      actual_WS = WS_TEST;
      actual_REDIRECT = REDIRECT_TEST;
    }
    else {
      actual_WS = WS_FULL;
      actual_REDIRECT = REDIRECT_FULL;      
    }
  }

  //constructor
  public CreatePayment(Gopayconfig gopayconfig) {
    //select from gopayconfig
    ESHOP_GOID = gopayconfig.getGoid();
    SECURE_KEY = gopayconfig.getSecurekey();
  }
  
  public void setOrderId(String orderId) {
    this.orderId = orderId;
  }
  
  public String getOrderId() {
    return orderId;
  }
  
  /**
   *
   * @return successElement
   */
  public EPaymentIdentity getEPaymentIdentity() {
    return successElement;
  }
  
  /**
   *
   * @param successURL
   * @param failedURL
   * @param totalPriceCents
   * @param productName
   * @param orderNumber
   * @param totalPrice
   * @param firstName
   * @param lastName
   * @param email
   * @param phoneNumber
   * @param street
   * @param p1
   * @param p2
   * @param p3
   * @param p4
   * @param city
   * @param postalCode
   * @return Long - Payment Session Id
   */
public EPaymentStatus createPayment(/*String successURL, // payment data
                          String failedURL,*/
                          String productName,
                          //Long targetGoId, -- no parameter
                          String orderNumber,
                          Long totalPriceCents,
                          //String currency, -- no parameter
                          //String encryptedSignature, -- no parameter
                          String firstName, // customer data
                          String lastName,
                          String email,
                          String phoneNumber,
                          String street,
                          String city,
                          String postalCode,
                          String p1, String p2,
                          String p3, String p4) throws GopayException, ServiceException {
  
  EPaymentStatus status = null;
  
  try { 
    
    provider = new AxisEPaymentProviderV2ServiceLocator().getEPaymentServiceV2(new URL(actual_WS));
    
    customerData = new ECustomerData();

    customerData.setFirstName(firstName);
    customerData.setLastName(lastName);
    customerData.setEmail(email);
    customerData.setPhoneNumber(phoneNumber);
    customerData.setStreet(street);
    customerData.setCity(city);
    customerData.setPostalCode(postalCode);
    customerData.setCountryCode(CUST_COUNTRY);

    paymentCommand = new EPaymentCommand();

    paymentCommand.setCustomerData(customerData);

    paymentCommand.setSuccessURL(success_URL);
    paymentCommand.setFailedURL(failed_URL);
    this.productName = productName;
    paymentCommand.setProductName(productName);
    paymentCommand.setTargetGoId(ESHOP_GOID);
    setOrderId(orderNumber);
    paymentCommand.setOrderNumber(orderNumber);

    this.totalPriceCents = totalPriceCents; // price is in Kč*100 (cents)
    paymentCommand.setTotalPrice(totalPriceCents);
    paymentCommand.setCurrency(CURRENCY);
    // set payment method
    paymentCommand.setPaymentChannels(ALLOWED_PAYMENT_CHANNELS);
    paymentCommand.setDefaultPaymentChannel(DEFAULT_PAYMENT_CHANNELS);
    paymentCommand.setLang(LANG);
    
    paymentCommand.setP1(p1);
    paymentCommand.setP2(p2);
    paymentCommand.setP3(p3);
    paymentCommand.setP4(p4);

    GopayHelper.signEPaymentCommand(paymentCommand, SECURE_KEY);

		status = provider.createPayment(paymentCommand);

			if (!EPaymentStatus.RESULT_CALL_COMPLETED.equals(status.getResult())) {				
				//status.getResultDescription() - popisuje chybovy stav
				throw new GopayException(GopayException.Reason.OTHER,
						"payment not created [" + status.getResultDescription() + "] ");
			}
			
			//Overeni podpisu
			GopayHelper.checkPaymentStatus(status, 
					ESHOP_GOID, 
					orderNumber, 
					totalPriceCents, 
					CURRENCY, 
					productName, 
					SECURE_KEY);

		} catch (RemoteException e) {
			throw new GopayException(GopayException.Reason.OTHER,
					"WS failure [" + e + "] ", e);

		} catch (MalformedURLException e) {
			throw new GopayException(GopayException.Reason.OTHER,
					"URL failure [" + e + "] ", e);
		}		
		return status;
	}

	/**
	 * Priprava formulare pro presmerovani na platebni branu GoPay. 
	 * 
	 * @return formular
	 */
	public String redirectToGW(Long paymentSessionId){
		
		EPaymentSessionInfo info = new EPaymentSessionInfo();
		info.setTargetGoId(ESHOP_GOID);
		info.setPaymentSessionId(paymentSessionId);
		GopayHelper.signEPaymentSession(info, SECURE_KEY);

		/*StringBuffer sb = new StringBuffer();		
		sb.append("<form action=\"" + actual_REDIRECT + "\" method=\"post\" >");		
		sb.append("<input name=\"sessionInfo.targetGoId\" value=\"" + String.valueOf(info.getTargetGoId()) + "\" type=\"hidden\">");
		sb.append("<input name=\"sessionInfo.paymentSessionId\" value=\"" + String.valueOf(info.getPaymentSessionId()) + "\" type=\"hidden\">");
		sb.append("<input name=\"sessionInfo.encryptedSignature\" value=\"" + String.valueOf(info.getEncryptedSignature()) + "\" type=\"hidden\">");
		sb.append("<input name=\"pay\" value=\"Zaplatit - úplná integrace\" type=\"submit\">");		        
		sb.append("</form>");*/
    
    System.out.println("REDIRECT ENCRYPTED SIGNATURE: [" + info.getEncryptedSignature() + "]");

    return /*sb.toString()*/ actual_REDIRECT +
           "?sessionInfo.paymentSessionId=" + info.getPaymentSessionId().toString() +
           "&sessionInfo.targetGoId =" + info.getTargetGoId().toString() +
           "&sessionInfo.encryptedSignature=" + info.getEncryptedSignature();
	}

  public EPaymentIdentity successRedirect(EPaymentStatus status/*Long paymentSessionId*/) throws ServiceException, GopayException {
    	//sestaveni navratovych hodnot pri presmerovani na successURL
			successElement = new EPaymentIdentity();
			successElement.setTargetGoId(ESHOP_GOID);
			successElement.setOrderNumber(orderId);
			successElement.setPaymentSessionId(status.getPaymentSessionId());
			successElement.setP1(status.getP1());
			successElement.setP2(status.getP2());
			successElement.setP2(status.getP3());
			successElement.setP2(status.getP4());
			GopayHelper.signEPaymentIdentity(successElement, SECURE_KEY);

			return successElement;			
  }
  
	/**  
	 * Provede overeni zaplacenosti objednavky po zpetnem presmerovani z platebni brany na success URL.
	 * 
   * @param identity
   * @return String "successURL" or "failedUR"
	 * @throws ServiceException 
	 * @throws GopayException 
	 */
	public String successUrl(EPaymentIdentity identity) throws ServiceException, GopayException {
    
    	String returnStatus = null;
			String message = null;
			String addMessage = null;

		try {
			
		  //kontrola parametru z redirectu - podpis, vazba na spravnou objednavku 
		  GopayHelper.checkPaymentIdentity(identity, ESHOP_GOID, orderId, SECURE_KEY);	    
			
			EPaymentSessionInfo info = new EPaymentSessionInfo();
			info.setTargetGoId(ESHOP_GOID);
			info.setPaymentSessionId(identity.getPaymentSessionId());
			GopayHelper.signEPaymentSession(info, SECURE_KEY);
      
      System.out.println("ENCRYPTED SIGNATURE IN successURL: [" + info.getEncryptedSignature() + "]");
				
			EPaymentStatus status = provider.paymentStatus(info);
	
			if (!EPaymentStatus.RESULT_CALL_COMPLETED.equals(status.getResult())) {
				throw new GopayException(GopayException.Reason.OTHER,
						"status failed [" + status.getResultDescription() + "] ");
			}
			//kontrola parametru ve stavu platby
			GopayHelper.checkPaymentStatus(status, 
					ESHOP_GOID, 
					orderId, 
					totalPriceCents, 
					CURRENCY, 
					productName, 
					SECURE_KEY);

      returnStatus = status.getSessionState();
      
		  System.out.println("PAYMENT STATUS IN successUrl method [paymentSessionId=" + status.getPaymentSessionId() + ", state="  + returnStatus + "]");

      if (EPaymentStatus.STATE_PAID.equals(status.getSessionState())) {
				//platba byla dokoncena - uhradte objednavku
				//prechazime na success url a prezentujeme message
				message = EPaymentConstants.msgReturnUrl(status.getSessionState());
				
			} else if (EPaymentStatus.STATE_PAYMENT_METHOD_CHOSEN.equals(status.getSessionState())) {
				//zakaznik uspesne zalozil platbu (napr. superCASH, bankovni platba) 
				message = EPaymentConstants.msgReturnUrl(status.getSessionState());				
				addMessage = EPaymentConstants.addMessage(status.getSessionSubState());
				
			} else if (EPaymentStatus.STATE_CANCELED.equals(status.getSessionState())) {
				//zakaznik provedl zruseni platby
				message = EPaymentConstants.msgReturnUrl(status.getSessionState());				
				
			} else {
				message = "Illegal state";				
				
			}
			//prechod na relevatni page + prezentace message	
			System.out.println("PAYMENT processing [message=" + message + ", add-message=" + addMessage + "]");
		} catch (Exception e) {
			System.out.println("Exception in successURL: [" + e + "]");
		}
    
    return returnStatus;
	}

	/**
	 * Zpracování návratu na failed URL, obsolete
	 * 
	 * @param identity
   * @return String "successURL" or "failedUR"
	 * @throws ServiceException
	 * @throws GopayException
	 */
	public String failedUrl(EPaymentIdentity identity) throws ServiceException, GopayException {
	    //kontrola parametru z redirectu - podpis, vazba na spravnou objednavku 
	    boolean retval = GopayHelper.checkPaymentIdentity(identity, ESHOP_GOID, orderId, SECURE_KEY);	    
      String failedState = "FAILED_URL_STATE";
	
      String message = "Platba byla zrušena";	

		//prechod na relevatni page + prezentace message	
		System.out.println("FW[" + failedState + "] " +
				"message[" + message + "]");
    
    return failedState;
	}
	
	/**
	 * Priprava overeni platby pred HTTP notifikaci
	 * 
   * @param identity
   * @param encryptedSignature
   * @param customerManager
   * @param customerId
	 * @throws ServiceException 
	 * @throws GopayException 
	 */
	public void prepareNotify(/*EPaymentIdentity identity,*/ String encryptedSignature,
                            CustomerOrderManager customerManager/*,
                            Customer customerId*/) /*throws ServiceException, GopayException*/ {
    
      // update signature
      customerManager.updateConfirmOrder(encryptedSignature);
	}
}
