/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author hariprasad
 */
@Entity
@Table(name = "gopayconfig")
@XmlRootElement
@NamedQueries({
  @NamedQuery(name = "Gopayconfig.findAll", query = "SELECT g FROM Gopayconfig g"),
  @NamedQuery(name = "Gopayconfig.findById", query = "SELECT g FROM Gopayconfig g WHERE g.id = :id"),
  @NamedQuery(name = "Gopayconfig.findByGoid", query = "SELECT g FROM Gopayconfig g WHERE g.goid = :goid"),
  @NamedQuery(name = "Gopayconfig.findBySecurekey", query = "SELECT g FROM Gopayconfig g WHERE g.securekey = :securekey")})
public class Gopayconfig implements Serializable {
  private static final long serialVersionUID = 1L;
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Basic(optional = false)
  @Column(name = "id")
  private Integer id;
  @Basic(optional = false)
  @NotNull
  @Column(name = "goid")
  private long goid;
  @Basic(optional = false)
  @NotNull
  @Size(min = 1, max = 45)
  @Column(name = "securekey")
  private String securekey;

  public Gopayconfig() {
  }

  public Gopayconfig(Integer id) {
    this.id = id;
  }

  public Gopayconfig(Integer id, long goid, String securekey) {
    this.id = id;
    this.goid = goid;
    this.securekey = securekey;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public long getGoid() {
    return goid;
  }

  public void setGoid(long goid) {
    this.goid = goid;
  }

  public String getSecurekey() {
    return securekey;
  }

  public void setSecurekey(String securekey) {
    this.securekey = securekey;
  }

  @Override
  public int hashCode() {
    int hash = 0;
    hash += (id != null ? id.hashCode() : 0);
    return hash;
  }

  @Override
  public boolean equals(Object object) {
    // TODO: Warning - this method won't work in the case the id fields are not set
    if (!(object instanceof Gopayconfig)) {
      return false;
    }
    Gopayconfig other = (Gopayconfig) object;
    if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
      return false;
    }
    return true;
  }

  @Override
  public String toString() {
    return "entity.Gopayconfig[ id=" + id + " ]";
  }
  
}
