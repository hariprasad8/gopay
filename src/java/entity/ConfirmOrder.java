/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author root
 */
@Entity
@Table(name = "custorder")
@XmlRootElement
@NamedQueries({
  @NamedQuery(name = "ConfirmOrder.findAll", query = "SELECT c FROM ConfirmOrder c"),
  @NamedQuery(name = "ConfirmOrder.findById", query = "SELECT c FROM ConfirmOrder c WHERE c.id = :id"),
  @NamedQuery(name = "ConfirmOrder.findByProductName", query = "SELECT c FROM ConfirmOrder c WHERE c.productName = :productName"),
  @NamedQuery(name = "ConfirmOrder.findByOrderNumber", query = "SELECT c FROM ConfirmOrder c WHERE c.orderNumber = :orderNumber"),
  @NamedQuery(name = "ConfirmOrder.findByTotalPrice", query = "SELECT c FROM ConfirmOrder c WHERE c.totalPrice = :totalPrice"),
  @NamedQuery(name = "ConfirmOrder.findByCurrency", query = "SELECT c FROM ConfirmOrder c WHERE c.currency = :currency"),
  @NamedQuery(name = "ConfirmOrder.findByPaymentSessionId", query = "SELECT c FROM ConfirmOrder c WHERE c.paymentSessionId = :paymentSessionId"),
  @NamedQuery(name = "ConfirmOrder.findByParentPaymentSessionId", query = "SELECT c FROM ConfirmOrder c WHERE c.parentPaymentSessionId = :parentPaymentSessionId"),
  @NamedQuery(name = "ConfirmOrder.findByEncryptedSignature", query = "SELECT c FROM ConfirmOrder c WHERE c.encryptedSignature = :encryptedSignature"),
  @NamedQuery(name = "ConfirmOrder.findByP1", query = "SELECT c FROM ConfirmOrder c WHERE c.p1 = :p1"),
  @NamedQuery(name = "ConfirmOrder.findByP2", query = "SELECT c FROM ConfirmOrder c WHERE c.p2 = :p2"),
  @NamedQuery(name = "ConfirmOrder.findByP3", query = "SELECT c FROM ConfirmOrder c WHERE c.p3 = :p3"),
  @NamedQuery(name = "ConfirmOrder.findByP4", query = "SELECT c FROM ConfirmOrder c WHERE c.p4 = :p4"),
  @NamedQuery(name = "ConfirmOrder.findByPaymentStatus", query = "SELECT c FROM ConfirmOrder c WHERE c.paymentStatus = :paymentStatus"),
  @NamedQuery(name = "ConfirmOrder.findByNotificationStatus", query = "SELECT c FROM ConfirmOrder c WHERE c.notificationStatus = :notificationStatus")})
public class ConfirmOrder implements Serializable {
  private static final long serialVersionUID = 1L;
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Basic(optional = false)
  @Column(name = "id")
  private Integer id;
  @Basic(optional = false)
  @NotNull
  @Size(min = 1, max = 256)
  @Column(name = "product_name")
  private String productName;
  @Basic(optional = false)
  @NotNull
  @Size(min = 1, max = 128)
  @Column(name = "order_number")
  private String orderNumber;
  @Basic(optional = false)
  @NotNull
  @Column(name = "total_price")
  private Long totalPrice;
  @Basic(optional = false)
  @NotNull
  @Size(min = 1, max = 45)
  @Column(name = "currency")
  private String currency;
  @Basic(optional = false)
  @NotNull
  @Column(name = "payment_session_id")
  private Long paymentSessionId;
  @Column(name = "parent_payment_session_id")
  private Long parentPaymentSessionId;
  @Size(min = 1, max = 2048)
  @Column(name = "encrypted_signature")
  private String encryptedSignature;
  @Size(max = 512)
  @Column(name = "p1")
  private String p1;
  @Size(max = 512)
  @Column(name = "p2")
  private String p2;
  @Size(max = 512)
  @Column(name = "p3")
  private String p3;
  @Size(max = 512)
  @Column(name = "p4")
  private String p4;
  @Size(max = 45)
  @Column(name = "payment_status")
  private String paymentStatus;
  @Column(name = "notification_status")
  private Integer notificationStatus;
  @Column(name = "datum")
  @Temporal(TemporalType.TIMESTAMP)
  private Date datum;
  @JoinColumn(name = "customer_id", referencedColumnName = "id", nullable = false)
  @ManyToOne(optional = false)
  private Customer customerId;

  public ConfirmOrder() {
  }

  public ConfirmOrder(Integer id) {
    this.id = id;
  }

  public ConfirmOrder(Integer id, String productName, String orderNumber, Long totalPrice, String currency, Long paymentSessionId, String encryptedSignature) {
    this.id = id;
    this.productName = productName;
    this.orderNumber = orderNumber;
    this.totalPrice = totalPrice;
    this.currency = currency;
    this.paymentSessionId = paymentSessionId;
    this.encryptedSignature = encryptedSignature;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getProductName() {
    return productName;
  }

  public void setProductName(String productName) {
    this.productName = productName;
  }

  public String getOrderNumber() {
    return orderNumber;
  }

  public void setOrderNumber(String orderNumber) {
    this.orderNumber = orderNumber;
  }

  public Long getTotalPrice() {
    return totalPrice;
  }

  public void setTotalPrice(Long totalPrice) {
    this.totalPrice = totalPrice;
  }

  public String getCurrency() {
    return currency;
  }

  public void setCurrency(String currency) {
    this.currency = currency;
  }

  public Long getPaymentSessionId() {
    return paymentSessionId;
  }

  public void setPaymentSessionId(Long paymentSessionId) {
    this.paymentSessionId = paymentSessionId;
  }

  public Long getParentPaymentSessionId() {
    return parentPaymentSessionId;
  }

  public void setParentPaymentSessionId(Long parentPaymentSessionId) {
    this.parentPaymentSessionId = parentPaymentSessionId;
  }
  
  public Date getDatum() {
    return datum;
  }

  public void setDatum(Date datum) {
    this.datum = datum;
  }


  public String getEncryptedSignature() {
    return encryptedSignature;
  }

  public void setEncryptedSignature(String encryptedSignature) {
    this.encryptedSignature = encryptedSignature;
  }

  public String getP1() {
    return p1;
  }

  public void setP1(String p1) {
    this.p1 = p1;
  }

  public String getP2() {
    return p2;
  }

  public void setP2(String p2) {
    this.p2 = p2;
  }

  public String getP3() {
    return p3;
  }

  public void setP3(String p3) {
    this.p3 = p3;
  }

  public String getP4() {
    return p4;
  }

  public void setP4(String p4) {
    this.p4 = p4;
  }

  public String getPaymentStatus() {
    return paymentStatus;
  }

  public void setPaymentStatus(String paymentStatus) {
    this.paymentStatus = paymentStatus;
  }

  public Integer getNotificationStatus() {
    return notificationStatus;
  }

  public void setNotificationStatus(Integer notificationStatus) {
    this.notificationStatus = notificationStatus;
  }
  
  public Customer getCustomerId() {
    return customerId;
  }

  public void setCustomerId(Customer customerId) {
    this.customerId = customerId;
  }

  @Override
  public int hashCode() {
    int hash = 0;
    hash += (id != null ? id.hashCode() : 0);
    return hash;
  }

  @Override
  public boolean equals(Object object) {
    // TODO: Warning - this method won't work in the case the id fields are not set
    if (!(object instanceof ConfirmOrder)) {
      return false;
    }
    ConfirmOrder other = (ConfirmOrder) object;
    if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
      return false;
    }
    return true;
  }

  @Override
  public String toString() {
    return "entity.ConfirmOrder[ id=" + id + " ]";
  }
  
}
