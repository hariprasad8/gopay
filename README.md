# **Aplikace GoPayYoga** #

* Aplikace GoPayYoga je interface do platebního portálu GoPay
* Version 1.0
*Aplikace byla vyvinuta v prostředí NetBeans v.8.0, Glassfish, MySQL.
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

1)  Skládá se z dvou částí (mohou být i odděleně ve dvou aplikacích). První část se týká registrace platebních údajů, jejich uložení do DB a přesměrování na platební bránu (virtuální banku) GoPay. Pokud je platba provedená, v tabulce **custorder** sloupec **payment_status** se objeví **"PAID"**. Jiný příznak evokuje, že platba nebyla provedena. Důvodů může být několik. Popsané jsou v dokumentaci ke GoPay.

2)  Druhá část obsahuje zpracování notifikace (upozornění, oznámení) o platbě. Platba se považuje za dokončenou, pokud přijde notifikace od GoPay. Tato část je asynchronní na části první a je ošetřena HTTP servletem. Pokud je platba úspěšně dokončená, v tabulce **custorder** sloupec **notification_status** se objeví **"1"**. Jiný příznak evokuje, že platba nebyla dokončená. Důvodů může být několik. Popsané jsou v dokumentaci ke GoPay, a v **session.CustomerOrderManager.
**
```
#!java

   * notificationResult         1 STATE_PAID (correct, passed, all right)
   *                            0 - FAILED (not known state, untreated state)
   *                            -1 STATE_CANCELED (wrong order, order canceled) 
   *                            -2 STATE_PAYMENT_METHOD_CHOSEN (payment was created, but not ended)
   *                            2 STATE_REFUNDED (this state is prepared for refund, but not implemented)
   *                            -3 STATE_TIMEOUTED (timeout)
   * notificationDescription    PASSED, FAILED, WRONGORDER - number of order is wrong
```

3) GoPay odesílá e-mail notifikaci klientovi (zákazníkovi), **pokud o to požádáme**, automaticky. Implementace notifikace - Http (např. servlet) nebo JAX-RPC (např. web-service) je ze strany GoPay požadovaná a bez tohoto kroku nemůžeme GoPay používat v produkční verzi. Použil jsem Http způsob z důvodu snadnější implementace.

4) Mail použitý v naší aplikaci je e-mail pro upozornění, že platba byla provedena, případně, že se něco přihodilo.  Nastavení našeho (interního) e-mailu je v ** mailer.EmailSessionBean** a texty při notifikaci, které mají být odeslány jsou v **session.CustomerOrderManager.notifyPayment**. Texty je potřeba upravit, původně měly být maily od nás odesílány také klientovi.

5) Aplikace je závislá na naší webové aplikaci určené pro klienty (cvičence), kteří se hlásí do kurzu, nebo na seminář. Notifikační část je určena naopak pro učetní kontrolu a jde tedy výhradně o interní službu (e-mail zákazníkovi odesílá GoPay). Dopručuji nějak v interní aplikaci zvýraznit, že platba prošla bez chyby (zelená fajfka), pokud je "payment_status = PAID" a "notification_status = 1", viz 1), 2).

6) Databáze obsahuje 3 tabulky, jejichž definice je v souboru **createJDZGOPAY.sql** viz **source**. Script byl vygenerován pomocí MySQL Workbench a je tedy určen pro MySQL. V případě jiné DB je potřeba provést úpravy v syntaxi ap:

```
#!SQL

-- -----------------------------------------------------
-- Table `jdzgopay`.`customer`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `jdzgopay`.`customer` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `surname` VARCHAR(45) NOT NULL,
  `email` VARCHAR(45) NULL,
  `telephone` VARCHAR(15) NULL,
  `address` VARCHAR(45) NOT NULL,
  `city` VARCHAR(45) NOT NULL,
  `zip` VARCHAR(8) NOT NULL,
  `datum` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_czech_ci
PACK_KEYS = DEFAULT;


-- -----------------------------------------------------
-- Table `jdzgopay`.`custorder`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `jdzgopay`.`custorder` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `product_name` VARCHAR(256) NOT NULL COMMENT 'productName from PaymentCommand',
  `order_number` VARCHAR(128) NOT NULL COMMENT 'orderNumber from PaymentCommand',
  `total_price` INT NOT NULL COMMENT 'totalPrice from PaymentCommand',
  `currency` VARCHAR(45) NOT NULL COMMENT 'currency from PaymentCommand',
  `payment_session_id` BIGINT NOT NULL COMMENT 'paymentSessionId from forwardURL method',
  `parent_payment_session_id` BIGINT NULL COMMENT 'parentPaymentSessionId from forwardURL method',
  `encrypted_signature` VARCHAR(2048) NOT NULL COMMENT 'encryptedSignature from PaymentCommand',
  `p1` VARCHAR(512) NULL COMMENT 'p1 from PaymentCommand',
  `p2` VARCHAR(512) NULL COMMENT 'p2 from PaymentCommand',
  `p3` VARCHAR(512) NULL COMMENT 'p3 from PaymentCommand',
  `p4` VARCHAR(512) NULL COMMENT 'p4 from PaymentCommand',
  `payment_status` VARCHAR(45) NULL COMMENT 'RESULT_CALL_COMPLETED = \"CALL_COMPLETED\",\nRESULT_CALL_FAILED = \"CALL_FAILED\",\nSTATE_CREATED = \"CREATED\",\nSTATE_PAYMENT_METHOD_CHOSEN = \"PAYMENT_METHOD_CHOSEN\",\nSTATE_AUTHORIZED = \"AUTHORIZED\",\nSTATE_PAID = \"PAID\",\nSTATE_CANCELED = \"CANCELED\",\nSTATE_TIMEOUTED = \"TIMEOUTED\",\nSTATE_REFUNDED = \"REFUNDED\"',
  `notification_status` INT NULL COMMENT '1 – Passed, -1 Failed, NULL - Not Passed',
  `datum` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `customer_id` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_order_customer`
    FOREIGN KEY (`customer_id`)
    REFERENCES `jdzgopay`.`customer` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_czech_ci;

CREATE UNIQUE INDEX `order_number_UNIQUE` ON `jdzgopay`.`custorder` (`order_number` ASC);

CREATE INDEX `fk_order_customer_idx` ON `jdzgopay`.`custorder` (`customer_id` ASC);


-- -----------------------------------------------------
-- Table `jdzgopay`.`gopayconfig`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `jdzgopay`.`gopayconfig` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `goid` BIGINT NOT NULL,
  `securekey` VARCHAR(45) NOT NULL,
  `appversion` VARCHAR(45) NULL,
  `datum` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_czech_ci
COMMENT = 'Table for common data';
```
Názvy sloupců byly voleny tak, aby nedocházelo ke kolizi s klíčovými slovy. (Je zajímavé, že i u tak málo tabulek jsem se asi 2x strefil. Raději jsem přejmenoval.) Velikost polí je volená s ohledem na specifikaci v dokumentaci GoPay. Nedoporučuji pole zbytečně zkracovat.

Tabulka **gopayconfig** obsahuje klíčové údaje pro správné šifrování, musí mít právě jeden řádek a všechna pole jsou povinná. Z bezpečnostních důvodů aktuální obsah 2 sloupců neuvádím. Ostaní sloupce: **id=1, appversion='1.0'**. Doporučuji udržovat verzi v **appversion** stejnou jako verzi aplikace v hlavním servletu, viz **VERSION** ** controller.ControllerServlet**. Datum by se mělo aktualizovat vždy při update jakéhokoli pole.

7) Testování se provádí v testovacím prostředí GoPay, ostrou produkční verzi jsem nezkoušel. Třída **controller.SwichFlag.java** obsahuje 3 přepínače, které mění nastavení:

```
#!java

// *************** for local testing purposes is true, for remote is false ***********
public static boolean testlocal = true;

// *************** for testing purposes is true, for production is false ***********
public static boolean testtest = true;

// *************** for local testing purposes to display .jsp page is true, for calling servlet directly is false ***********
public static boolean displaypage = true;


/* possible combinations ad meaning
testlocal testtest displaypage
false     false    false        remote used production version (not recomended)
true      true     true/false   testing locally
false     true     true/false   remote testing
true      false    true/false   locally used production version
*/
```
* Přepínač **testlocal** nastavuje, zda je aplikace testovaná/provozovaná lokálně, nebo vzdáleně. Nastavuje správně URL adresu v ** controller.ControllerServlet**:

```
#!java

  public static final String 
               externRootURL = "http://172.213.broadband10.iol.cz:8080/",
               internRootURL =  "http://localhost:8080/",
               APP_URL = "gopayyoga/";

```
Přepište nastavení podle aktuální adresy.

* Přepínač **testtest** přepíná mezi testovacím a produkčním prostředím GoPay.

* Přepínač **displaypage** je obsolete, servlet nerozlišuje, jestli je zavolaný Get/Post nebo Request/Response.

8) Deployment je záležítostí prostředí, ve kterém je aplikace nasazená. Je striktně doporučeno, kvůli bezpečné komunikaci mezi webovou aplikací a touto aplikací prostřednictvím HTTP protokolu, aby obě aplikace běžely na stejném serveru URL adresa pro zavolání této aplikace by pak vypadala následovně: 
```
#!html

https://localhost:8181/gopayyoga/gopay?name=Jan&surname=Halasa&email=henrich108@gmail.com&phone=123456789&address=Programovacia25&city=Prespurk&zip=12300&product=Seminar&price=10&surcharge=0&p1=&p2=&p3=&p4=&orderNumber=000101

nebo

http://localhost:8080/gopayyoga/gopay?name=Jan&surname=Halasa&email=henrich108@gmail.com&phone=123456789&address=Programovacia25&city=Prespurk&zip=12300&product=Seminar&price=10&surcharge=0&p1=&p2=&p3=&p4=&orderNumber=000101
```
V tomto případě udělá aplikace loopback jen přes síťovou kartu a vzájemné http volání je zvenčí nezjistitelné, proto je použití šifrovaného URL zbytečné. Volání forward stránky GoPay a zpětné přesměrování se provádí z důvodů problémů s certifikáty nešifrovaně. Bezpečnost je zajištěná šifrovaným podpisem. Povinné parametry jsou **name**, **surname**, **email**, **address**, **city**, **zip**, **product**, **price**, **surcharge**, **orderNumber**. Aplikace nekontoluje sémantickou správnost polí:

-    **email** musí být platný e-mail.
-  **price** musí být ve tvaru XXXXXX.XX (v Kč), je akceptovaná desetinná tečka. Cena je převedená na BigDecimal (zachovává přesná čísla, což Double ap. nezaručuje), a po navýšení o procenta (**surcharge**) je přepočítána a zaokrouhlená na haléře a uložená v DB jako Integer.
-  **surcharge** bude zpravidla nula, akceptovatelné je opět číslo ve stejném tvaru jako **price**, tj. včetně desetinných míst.
-  **orderNumber** je popsáno níže
- pomocná pole **p1**-**p4** jsou nepovinná a slouží například k poznámkám nebo k odeslání nějakých specifických informací (velikost je 512B).

### Další poznámky k bodům ###

* Generovaný Order Number (variabilní symbol) je velmi důležitá položka (unique v batabázi). Je tedy potřeba, aby byl opravdu unikátní. Současná praxe je taková, že variabilní symbol se generuje z prvních tří čísel, kde první číslice (1,2) rozlišuje kurz, seminář, další 2 číslice udávají lokaci (např. Velehradská je 02), další čísla jsou pořadí, tedy první klient ve Velehradské má v.s. například 102001 (nebo 102000). Potíž je v tom, že pořadí od 0 do 999 je vypotřebováno docela rychle a bankovní variabilní symbol je jen šestimístný. Navrhuji ošetřit Order Number tak, že variabilní symbol se doplní o DDMMYYYYHH24MISS. Pro jednodušší použití třeba za pomlčítkem. Tedy první klient ze dne 1.9.2014 13:30:01 bude mít v.s. 102001 a order_number=102001-01092014133001.
* Sahání z webové klientské aplikace. Klienstká aplikace si možná bude chtít ověřit Order Number, ale v zásadě by to být nemuselo. Způsob generování popsaný výše by měl být jednoznačný.
* Kontrola z interní aplikace (YORK) viz bod 5)

* Repo owner or admin: Hariprasad, petr.hariprasad@gmail.com
* Other community or team contact: naradapuri@gmail.com