SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

CREATE SCHEMA IF NOT EXISTS `jdzgopay` DEFAULT CHARACTER SET utf8 COLLATE utf8_czech_ci ;
USE `jdzgopay` ;

-- -----------------------------------------------------
-- Table `jdzgopay`.`customer`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `jdzgopay`.`customer` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `surname` VARCHAR(45) NOT NULL,
  `email` VARCHAR(45) NULL,
  `telephone` VARCHAR(15) NULL,
  `address` VARCHAR(45) NOT NULL,
  `city` VARCHAR(45) NOT NULL,
  `zip` VARCHAR(8) NOT NULL,
  `datum` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_czech_ci
PACK_KEYS = DEFAULT;


-- -----------------------------------------------------
-- Table `jdzgopay`.`custorder`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `jdzgopay`.`custorder` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `product_name` VARCHAR(256) NOT NULL COMMENT 'productName from PaymentCommand',
  `order_number` VARCHAR(128) NOT NULL COMMENT 'orderNumber from PaymentCommand',
  `total_price` INT NOT NULL COMMENT 'totalPrice from PaymentCommand',
  `currency` VARCHAR(45) NOT NULL COMMENT 'currency from PaymentCommand',
  `payment_session_id` BIGINT NOT NULL COMMENT 'paymentSessionId from forwardURL method',
  `parent_payment_session_id` BIGINT NULL COMMENT 'parentPaymentSessionId from forwardURL method',
  `encrypted_signature` VARCHAR(2048) NOT NULL COMMENT 'encryptedSignature from PaymentCommand',
  `p1` VARCHAR(512) NULL COMMENT 'p1 from PaymentCommand',
  `p2` VARCHAR(512) NULL COMMENT 'p2 from PaymentCommand',
  `p3` VARCHAR(512) NULL COMMENT 'p3 from PaymentCommand',
  `p4` VARCHAR(512) NULL COMMENT 'p4 from PaymentCommand',
  `payment_status` VARCHAR(45) NULL COMMENT 'RESULT_CALL_COMPLETED = \"CALL_COMPLETED\",\nRESULT_CALL_FAILED = \"CALL_FAILED\",\nSTATE_CREATED = \"CREATED\",\nSTATE_PAYMENT_METHOD_CHOSEN = \"PAYMENT_METHOD_CHOSEN\",\nSTATE_AUTHORIZED = \"AUTHORIZED\",\nSTATE_PAID = \"PAID\",\nSTATE_CANCELED = \"CANCELED\",\nSTATE_TIMEOUTED = \"TIMEOUTED\",\nSTATE_REFUNDED = \"REFUNDED\"',
  `notification_status` INT NULL COMMENT '1 – Passed, -1 Failed, NULL - Not Passed',
  `datum` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `customer_id` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_order_customer`
    FOREIGN KEY (`customer_id`)
    REFERENCES `jdzgopay`.`customer` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_czech_ci;

CREATE UNIQUE INDEX `order_number_UNIQUE` ON `jdzgopay`.`custorder` (`order_number` ASC);

CREATE INDEX `fk_order_customer_idx` ON `jdzgopay`.`custorder` (`customer_id` ASC);


-- -----------------------------------------------------
-- Table `jdzgopay`.`gopayconfig`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `jdzgopay`.`gopayconfig` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `goid` BIGINT NOT NULL,
  `securekey` VARCHAR(45) NOT NULL,
  `appversion` VARCHAR(45) NULL,
  `datum` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_czech_ci
COMMENT = 'Table for common data';


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
